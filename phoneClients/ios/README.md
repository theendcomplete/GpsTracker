PLEASE NOTE - THIS APP IS CURRENTLY BEING REWRITTEN.

It is functional but only the start tracking button is working. The values for default upload website and username must be hardcoded in WSViewController.m.

Remember that you need to open this project with GpsTracker.xcworkspace since you are using AFNetworking cocoapods.

use the websmithing defaultUploadWebsite:

https://www.websmithing.com/gpstracker/updatelocation.php

for testing 

change the *phoneNumber* form variable to something you know and then check your location with your browser here: 
 
https://www.websmithing.com/gpstracker/displaymap.php
